package Work1;

public class Main {
	public static void main(String[] args) {
		
		Car c1 = new Car("BOOM");
		Honda h1 = new Honda("HUMVY");
		Lamborghini l1 = new Lamborghini("AVENTADOR");
		
		System.out.println(c1.getName());
		System.out.println(h1.getName());
		System.out.println(l1.getName());
	}

}
