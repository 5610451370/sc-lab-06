package Work2;

public class Main {
	
	public static void main(String[] args) {
		
		Car car = new Car("Ferrari F430", 1000000 );
		Motorcycle motor = new Motorcycle("Ducati", 500000 );
		MechanicCentre mc = new MechanicCentre();
		
		System.out.println("Insurance of Car is : "+mc.checkInsurance(car));
		System.out.println("Insurance of Motorcycle is : "+mc.checkInsurance(motor));
		System.out.println("Insurance of General Vehicle is : "+mc.checkInsurance((Vehicle)car));
	}
	
			
}
