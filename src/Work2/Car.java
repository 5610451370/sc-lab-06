package Work2;

public class Car extends Vehicle {


	public Car(String name, int cost) {
		super(name, cost);
	}

	@Override
	public int inSure() {
		return 4;
	}

	@Override
	public String oil() {
		return "Benzine or NGV-Gas or LPG-Gas";
	}
	
	

}
