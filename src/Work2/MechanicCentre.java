package Work2;

public class MechanicCentre {
	
	public int checkInsurance(Car c) {
		return c.inSure();
	}
	
	public int checkInsurance(Motorcycle m) {
		return m.inSure();
	}
	
	public int checkInsurance(Vehicle v) {
		return v.inSure();
	}
	
	

}
