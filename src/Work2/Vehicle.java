package Work2;

public abstract class Vehicle {
	
	private String name;
	private int cost;
	
	public Vehicle(String name, int cost) {
		this.name = name;
		this.cost = cost;
	}
	
	public abstract int inSure();
	
	public abstract String oil();
	
	public String getName() {
		return this.name;
	}

}
